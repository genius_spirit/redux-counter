import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://blog-187e7.firebaseio.com/'
});

export default instance;
