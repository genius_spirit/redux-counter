import React, { Component } from 'react';
import {connect} from 'react-redux';
import './Counter.css';
import { addCounter, decreaseCounter, fetchCounter, increaseCounter, saveCounter, subtractCounter } from "../../store/actions";

class Counter extends Component {
  componentDidMount() {
    this.props.fetchCounter();
  }

  render() {
    return(
      <div className="Counter">
        <h1>{this.props.counter}</h1>
        <button onClick={this.props.increaseCounter}>Increment</button>
        <button onClick={this.props.decreaseCounter}>Decrement</button>
        <button onClick={this.props.addCounter}>Increase by 5</button>
        <button onClick={this.props.subtractCounter}>Decrease by 5</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    counter: state.counter
  }
};

const mapDispatchToProps = dispatch => {
  return {
    increaseCounter: () => {dispatch(increaseCounter()); dispatch(saveCounter())},
    decreaseCounter: () => {dispatch(decreaseCounter()); dispatch(saveCounter())},
    addCounter: () => dispatch(addCounter(5)),
    subtractCounter: () => {dispatch(subtractCounter(5)); dispatch(saveCounter())},
    fetchCounter: () => dispatch(fetchCounter()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);