import {ADD, DECREMENT, FETCH_COUNTER_SUCCESS, INCREMENT, SUBTRACT} from "./actions";

const initialState = {
  counter: 0
};

const reducer = (state = initialState, action) => {

  switch (action.type) {
    case INCREMENT:
      return {counter: state.counter + 1};
    case DECREMENT:
      return {counter: state.counter - 1};
    case ADD:
      return {counter: state.counter + action.amount};
    case SUBTRACT:
      return {counter: state.counter - action.amount};
    case FETCH_COUNTER_SUCCESS:
      return {counter: action.counter};
    default:
      return state;
  }

};

export default reducer;